# On the workstation

scp -P 4242 ./exploit-getuid.c level13@localhost:/tmp/exploit-getuid.c

ssh -p 4242 level13@localhost 'bash -s' << 'EOF'
cd /tmp
gcc -fPIC -shared -o /tmp/lib.so /tmp/exploit-getuid.c

cp ~/level13 /tmp/level13
LD_PRELOAD="/tmp/lib.so" /tmp/level13
EOF