# SnowCrash
## Projet SnowCrash | Projet Sécurité | Ecole 42
### Résumé:<br>
* Ce projet est une introduction à la sécurité en informatique. *

## Objectifs
Ce projet a pour but de nous faire découvrir, via plusieurs petits challenges, la sécurité
en informatique dans plusieurs domaines.
<br>
Les méthodes que nous allons utiliser, plus ou moins complexes, nous feront voir différemment l’informatique en général.
<br>
Durant ce projet, nous allons devoir trouver la faille de chaque niveau afin de pouvoir passer au niveau suivant.
Chaque faille nous donnera un mot de passe, afin de se connecter avec l'utilisateur flagXX et de recuperer le flag avec le binaire getflag ou alors il est possible de trouver directement le flag et de se connecter au niveau suivant.

Il est possible de trouver dans les différents dossiers la manière dont nous nous y sommes pris afin de trouver les failles et de les exploiter.
