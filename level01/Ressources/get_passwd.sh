# On workstation

scp -P 4242 level01@localhost:/etc/passwd .

cat << EOF | docker run -v $(pwd):/level01 -i kalilinux/kali-rolling
cd /level01
apt-get update
apt-get install -y john
john /level01/passwd
john /level01/passwd --show > /level01/result.txt
cat /level01/result.txt
EOF